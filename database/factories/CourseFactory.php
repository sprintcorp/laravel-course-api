<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Course;
use Faker\Generator as Faker;

$factory->define(Course::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'description' => $faker->realText(100),
        'course_code' => $faker->unique()->randomNumber,
        'unit' => $faker->randomNumber(1)           
    ];
});