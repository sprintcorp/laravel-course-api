<?php

namespace App\Traits;

use App\Http\Resources\CourseResource;
use App\Http\Resources\UserCourseResource;



trait ApiResponse
{
    protected function successResponse($data,$code = 200)
    {
        return response()->json($data,$code);
    }

    protected function errorResponse($message,$code)
    {
        return response()->json(['error' => $message,'code'=>$code],$code);
    }

    protected function courses($data,$code = 200){
        return $this->successResponse(CourseResource::collection($data),$code);
    }
    protected function user_courses($data,$code = 200){
        return $this->successResponse(UserCourseResource::collection($data),$code);
    }

    protected function course($data,$code = 200){
       
        return $this->successResponse(new CourseResource($data),$code);
    }
}