<?php

namespace App;

use App\UserCourse;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $guarded = [];
    protected $table = 'courses';
    
    public function user_course()
    {
        return $this->hasMany(UserCourse::class);
    }
}