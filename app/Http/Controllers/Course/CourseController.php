<?php

namespace App\Http\Controllers\Course;

use App\Course;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use App\Exports\CourseExport;
use Illuminate\Support\Carbon;
use App\Http\Controllers\Controller;
use App\Http\Requests\CourseRequest;
use Maatwebsite\Excel\Facades\Excel;

class CourseController extends Controller
{
    use ApiResponse;
    public function __construct()
    {
        $this->middleware('jwt');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->courses(Course::latest()->get());
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $course = factory(Course::class,50)->create();
        return $this->courses($course);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
    
    /**
    * @return \Illuminate\Support\Collection
    */
    public function export() 
    {
        $date = Carbon::now()->format("d-m-y-h-m-s");
        $course = Excel::store(new CourseExport, 'export\course'.$date.'.xlsx');
        return $course ? $this->successResponse("Course Exported. Check storage \ app \ export to view exported file") :$this->errorResponse("Course not Exported",500);
    }
}