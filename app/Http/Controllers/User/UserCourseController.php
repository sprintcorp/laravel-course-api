<?php

namespace App\Http\Controllers\User;

use App\Course;
use App\UserCourse;
use App\Traits\ApiResponse;
use Illuminate\Http\Request;
use App\Service\CourseRegister;
use App\Http\Controllers\Controller;

class UserCourseController extends Controller
{
    use ApiResponse;
    public function __construct()
    {
        $this->middleware('jwt');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_course = $this->user_courses(UserCourse::where('user_id',auth()->user()->id)->get());
        $course = $this->courses(Course::get());  
        return $course;
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $registration = new CourseRegister();
        return $registration->RegisterCourse($data) ? $this->successResponse("Course Registration Successful",200) 
                : $this->errorResponse("Error in registration process",500);   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}