<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|unique:courses|string|max:100',
            'description' => 'required|string|email|max:255',
            'course_code' => 'required|string|max:50',
            'unit' => 'required|integer|max:50',
        ];
    }

        /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'name is required',
            'description.required' => 'description is required',
            'course_code.required' => 'course code is required',
            'name.unique' => 'course name must be unique',
            'unit.required' => 'course unit is required',
        ];
    }
}