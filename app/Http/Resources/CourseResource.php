<?php

namespace App\Http\Resources;

use App\UserCourse;
use Illuminate\Http\Resources\Json\JsonResource;

class CourseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
            return[
            'id'=> $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'unit' => $this->unit,
            'course_code' => $this->course_code,
            'registered' => UserCourse::where('course_id',$this->id)->where('user_id',auth()->user()->id)->first() ?'Registered on '. $this->user_course->first()->created_at->format('Y-m-d'):'Not Registered',
            'created_at' => $this->created_at->diffForHumans(),
            ];
    }
}