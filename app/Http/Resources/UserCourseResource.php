<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserCourseResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'id'=> $this->id,
            'name' => $this->course->name,
            'description' => $this->course->description,
            'unit' => $this->course->unit,
            'course_code' => $this->course->course_code,
            'registered' => $this->created_at->diffForHumans(),
            ];
    }
}