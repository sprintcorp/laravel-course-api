<?php

namespace App\Service;

use App\UserCourse;

class CourseRegister{
    
    public function RegisterCourse($data){
        $courses = array_map('intval', explode(',', $data['course_id']));        
        foreach($courses as $course){
            $user_course = new UserCourse();
            $user_course->user_id = $data['user_id'];
            $user_course->course_id = $course;
            $user_course->save();
        }
        return true;
    }
}