<?php

namespace App\Exceptions;

use Exception;
use App\Traits\ApiResponse;
use Illuminate\Database\QueryException;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Validation\ValidationException;
use Illuminate\Auth\Access\AuthorizationException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\MethodNotAllowedHttpException;

class Handler extends ExceptionHandler
{
    use ApiResponse;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception)
    {
        if($exception instanceof TokenInvalidException){
            return $this->errorResponse('Token is invalid',400);
        }
        if($exception instanceof TokenExpiredException){
            return $this->errorResponse('Token expired',404);
        }
        if($exception instanceof JWTException){
            return $this->errorResponse('There is a problem with your token',404);
        }
        if($exception instanceof QueryException){
            return $this->errorResponse($exception->getMessage(),500);
        }
        if($exception instanceof AuthorizationException){
            return $this->errorResponse($exception,401);
        }
        if($exception instanceof AccessDeniedHttpException){
            return $this->errorResponse($exception->getMessage(),403);
        }
        if($exception instanceof NotFoundHttpException){
            return $this->errorResponse($exception->getMessage(),404);
        }
        if($exception instanceof MethodNotAllowedHttpException){
            return $this->errorResponse($exception->getMessage(),405);
        }
        
        if($exception instanceof AuthenticationException){
            return $this->unauthenticated($request,$exception);
        }
        
        if($exception instanceof AuthorizationException){
            return $this->errorResponse($exception->getMessage(),403);
        }
       //Handle Unexpected exception
       if(config('app.debug')) {
        return parent::render($request, $exception);
        }
        return $this->errorResponse('Unexpected Exception. Try later',500);
    }

     /**
     * Convert an authentication exception into a response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Illuminate\Auth\AuthenticationException  $exception
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return $this->errorResponse('unauthenticated '.$exception->getLine(),401);
    }
}