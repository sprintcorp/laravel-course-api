<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserCourse extends Model
{
    protected $guarded = [];
    protected $table = 'user_course';

    public function user()
    {
        return $this->belongsTo(User::class,'user_id');
    }
    
    public function course()
    {
        return $this->belongsTo(Course::class,'course_id');
    }

    public function check_user_registration($course){
        return $this::where('course_id',$course)->where('user_id',auth()->user()->id)->first();
    }
}